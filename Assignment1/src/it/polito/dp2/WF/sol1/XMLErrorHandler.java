package it.polito.dp2.WF.sol1;

import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLErrorHandler extends DefaultHandler {

	public void fatalError(SAXParseException e) throws SAXParseException {
		System.err.println(e.getSystemId() 
				+ ":" + e.getLineNumber()
				+ ": error: " + e.getMessage());

		throw e;
	}
	
	// All errors are fatal
	public void error(SAXParseException e) throws SAXParseException {
		System.err.println(e.getSystemId() 
				+ ":" + e.getLineNumber()
				+ ": error: " + e.getMessage());

		throw e;
	}

	// Print warnings and continue to parse
	public void warning(SAXParseException e) throws SAXParseException {
		System.err.println(e.getSystemId() 
				+ ":" + e.getLineNumber()
				+ ": warning: " + e.getMessage());
	}
}
