package it.polito.dp2.WF.sol1;

import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import it.polito.dp2.WF.ActionReader;
import it.polito.dp2.WF.Actor;
import it.polito.dp2.WF.WorkflowMonitorException;
import it.polito.dp2.WF.WorkflowReader;

public class XMLWorkflowMonitorParser {
	private static final int ENCLOSING_WORKFLOW_INDEX	= 0; 
	private static final int ACTION_WORKFLOW_INDEX		= 1; 
	private static final int PROCESS_ACTION_INDEX 		= 2;
	
	private static Logger logger = Logger.getLogger(XMLWorkflowMonitorParser.class.getName());
	
	private static DateFormat dateFormat;
	
	private WorkflowMonitorFactory wfFactory;
	private WorkflowMonitorImplementation monitor;
	private DocumentBuilderFactory dBFactory;
	private DocumentBuilder builder;
	private HashMap<String, String> unsolvedNextActions;
	private LinkedList<String[]> unsolvedActionWorkflows;
	private HashSet<String> workflowsNames;
	
	public XMLWorkflowMonitorParser() throws WorkflowMonitorException {
		Resources.setLogHandler(logger, true);
		try {
			dateFormat 	= new SimpleDateFormat(Resources.DATE_FORMAT);
			wfFactory 	= new WorkflowMonitorFactory();
			monitor 	= (WorkflowMonitorImplementation) wfFactory.newWorkflowMonitor();
			
			dBFactory = DocumentBuilderFactory.newInstance();
			dBFactory.setValidating(true);
			
			builder = dBFactory.newDocumentBuilder();
			builder.setErrorHandler(new XMLErrorHandler());
			
			unsolvedNextActions 	= new HashMap<String, String>();
			unsolvedActionWorkflows = new LinkedList<String[]>();
		} catch (WorkflowMonitorException wme) {
			logger.severe(wme.getMessage());
			throw wme;
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new WorkflowMonitorException(e);
		} 
	}
	
	public XMLWorkflowMonitorParser(WorkflowMonitorImplementation monitor) throws WorkflowMonitorException {
		Resources.setLogHandler(logger, true);
        try {
        	dateFormat = new SimpleDateFormat(Resources.DATE_FORMAT);
			this.monitor = monitor;
			
			dBFactory = DocumentBuilderFactory.newInstance();
        	dBFactory.setValidating(true);
        	
			builder = dBFactory.newDocumentBuilder();
			builder.setErrorHandler(new XMLErrorHandler());
			
			unsolvedNextActions 	= new HashMap<String, String>();
			unsolvedActionWorkflows = new LinkedList<String[]>();
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new WorkflowMonitorException(e);
		}
	}
	
	public void parse(String fileName) throws WorkflowMonitorException {
		logger.info("Starting deserializer on file " + fileName + " ...");
		try {
//			Document document = builder.parse(new FileInputStream(Resources.XML_FILE_NAME));
			Document document = builder.parse(new FileInputStream(fileName));
//    		Element rootElement = document.getDocumentElement();	// workflowMonitor
    		NodeList workflowsList = document.getElementsByTagName(Resources.WORKFLOW);
    		NodeList processesList = document.getElementsByTagName(Resources.PROCESS);
    		
    		populateSet(workflowsList);
    		parseWorkflows(workflowsList);
    		solveActionWorkflows();
    		parseProcesses(processesList);
    	
		} catch (WorkflowMonitorException wme) {
			logger.severe(wme.getMessage());
			throw wme; 
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new WorkflowMonitorException(e); 
		} finally {
			logger.info("Deserializer terminated");			
		}
	}
	
	public void parseWorkflows(NodeList workflowsList) throws WorkflowMonitorException {
		int length = workflowsList.getLength();
		logger.info("Parsing " + length + " workflows...");
		for (int i = 0; i < length; i++) {
			Element workflowNode = (Element) workflowsList.item(i);
			parseWorkflow(workflowNode);				
		}
		logger.info("Workflows correctly parsed.");
	}
	
	public void parseProcesses(NodeList processesList) throws WorkflowMonitorException {
		int length = processesList.getLength();
		logger.info("Parsing " + length + " processes...");
		for (int i = 0; i < length; i++) {
			Element processNode = (Element) processesList.item(i);
			parseProcess(processNode);				
		}
		logger.info("Processes correctly parsed");
	}
	
	public void parseWorkflow(Element workflowNode) throws WorkflowMonitorException {
		String name = workflowNode.getAttribute(Resources.NAME); // <-- controllo robustezza presente in populateSets
		Workflow workflow = new Workflow(name);
		monitor.addWorkflow(workflow);
		
		logger.info("Parsing workflow " + name + "...");
		
		NodeList actions = workflowNode.getChildNodes();    				
		int actionsNumber = actions.getLength();
		if (actionsNumber > 0) {
			for (int j = 0; j < actionsNumber; j++) {
				Node action = actions.item(j);
				if (action.getNodeType() == Node.ELEMENT_NODE && action.getNodeName().equals(Resources.ACTION)) {
					parseAction((Element) action); 
				}
			} 
			
			for (Entry<String, String> entry : unsolvedNextActions.entrySet()) {
		    	SimpleAction action = (SimpleAction) workflow.getAction(entry.getKey());
		    	String[] nextActions = entry.getValue().split(" ");
		    	for (int k = 0; k < nextActions.length; k++) {
		    		String nextActionName = nextActions[k];
		    		if (!nextActionName.matches(Resources.NAME_REGEX))
		    	    	throw new WorkflowMonitorException("Invalid next action name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		    		
		    		Action a = (Action) workflow.getAction(nextActionName);
		    		if (a == null)
		    			throw new WorkflowMonitorException("Node " + action.getName() + ": invalid name for next action " + nextActions[k] + ", action not exists.");
					
		    		action.addPossibleNextAction(a);
				}
		    }
			unsolvedNextActions.clear();
		}
		
		logger.info("Workflow " + name + " correctly parsed");
	}
	
	public void parseProcess(Element processNode) throws WorkflowMonitorException {
		String workflowName = processNode.getAttribute(Resources.WORKFLOW);
		if (workflowName == null || workflowName.isEmpty())
			throw new WorkflowMonitorException("Node " + processNode.getNodeName() + ": " + Resources.WORKFLOW + " attribute not found or have no value.");
		if (!workflowName.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Node " + processNode.getNodeName() + ": invalid " + Resources.NAME + " format, it must be a string made of alphanumeric characters only, where the first character is alphabetic." + workflowName);
		if (!workflowsNames.contains(workflowName))
			throw new WorkflowMonitorException("Node " + processNode.getNodeName() + ": " + Resources.WORKFLOW + " '" + workflowName + "' not exists.");
		
		String startTime = processNode.getAttribute(Resources.START_TIME);
		if (startTime == null || startTime.isEmpty())
			throw new WorkflowMonitorException("Node " + processNode.getNodeName() + ": " + Resources.START_TIME + " attribute not found or have no value.");
			
		logger.info("Parsing process of workflow " + workflowName + " started at " + startTime + "...");
		
		Calendar calendar = new GregorianCalendar();
		try {
			calendar.setTime(dateFormat.parse(startTime));
			calendar.setTimeZone(dateFormat.getTimeZone());
		} catch (ParseException e) {
			throw new WorkflowMonitorException("Node " + processNode.getNodeName() + ": Invalid date time format, expected '" + Resources.DATE_FORMAT + "'.");
		}
		
		WorkflowReader workflow = monitor.getWorkflow(workflowName);
		Process process = new Process(calendar, workflow);
		((Workflow) workflow).addProcess(process);
		monitor.addProcess(process);
		
		NodeList actionsStatus = processNode.getChildNodes();    				
		int actionsStatusNumber = actionsStatus.getLength();
		if (actionsStatusNumber > 0) {
			for (int j = 0; j < actionsStatusNumber; j++) {
				Node actionStatus = actionsStatus.item(j);
				if (actionStatus.getNodeType() == Node.ELEMENT_NODE && actionStatus.getNodeName().equals(Resources.ACTION_STATUS)) {
					parseActionStatus((Element) actionStatus, process, workflow); 
				}
			}
		}
		
		logger.info("Process of workflow " + workflowName + " started at " + startTime + " correctly parsed");
	}
	
	public void parseAction(Element actionNode) throws WorkflowMonitorException {
	    if (!actionNode.hasAttributes())
	    	throw new WorkflowMonitorException("Node " + actionNode.getNodeName() + ": no attribute found.");
	    
	    String name = actionNode.getAttribute(Resources.NAME);
	    if (name == null || name.isEmpty())
	    	throw new WorkflowMonitorException("Node " + actionNode.getNodeName() + ": " + Resources.NAME + " attribute not found or have no value.");
	    if (!name.matches(Resources.NAME_REGEX))
	    	throw new WorkflowMonitorException("Node " + actionNode.getNodeName() + ": invalid " + Resources.NAME + " format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
	    
	    logger.info("Parsing action " + name + "...");
	    
	    String role = actionNode.getAttribute(Resources.ROLE);
	    if (role == null || role.isEmpty())
	    	throw new WorkflowMonitorException("Node " + actionNode.getNodeName() + ": " + Resources.ROLE + " attribute not found or have no value.");
	    if (!role.matches(Resources.ROLE_REGEX))
	    	throw new WorkflowMonitorException("Node " + actionNode.getNodeName() + ": invalid " + Resources.ROLE + " format, it must be an alphabetic string.");
	    
	    String enclosingWorkflowName = actionNode.getAttribute(Resources.ENCLOSING_WORKFLOW);
	    if (enclosingWorkflowName == null || enclosingWorkflowName.isEmpty())
	    	throw new WorkflowMonitorException("Node " + actionNode.getNodeName() + ": " + Resources.ENCLOSING_WORKFLOW + " attribute not found or have no value.");
	    
	    WorkflowReader enclosingWorkflow = monitor.getWorkflow(enclosingWorkflowName);
	    
	    String automaticallyInstantiated = actionNode.getAttribute(Resources.IS_AUTOMATICALLY_INSTANTIATED);
	    boolean isAutomaticallyInstantiated = (automaticallyInstantiated.isEmpty())? false : Boolean.parseBoolean(automaticallyInstantiated);
	    
	    Node node = actionNode.getFirstChild();
	    boolean found = false;
	    while (node != null) {
	    	if (node.getNodeType() == Node.ELEMENT_NODE) {
				found = true;
				break;
			}
			node = node.getNextSibling();
		}
	    
	    if (!found)
	    	throw new WorkflowMonitorException("Node " + actionNode.getNodeName() + ": kind of action not present, expected " + Resources.SIMPLE_ACTION + " or " + Resources.PROCESS_ACTION + ".");
	    
	    Element actionType = (Element) node;
	    
	    if (actionType.getNodeName().equals(Resources.SIMPLE_ACTION)) {
	    	logger.info("Action " + name + " is a simple action");
	    	SimpleAction action = new SimpleAction(name, role, enclosingWorkflow, isAutomaticallyInstantiated);
			String nextActions = actionType.getAttribute(Resources.NEXT_ACTIONS);
//				throw new WorkflowMonitorException("Node " + actionType.getNodeName() + ": null or invalid attribute value, expected string(s).");
			if (nextActions != null && !nextActions.isEmpty())
				unsolvedNextActions.put(name, nextActions);
			
			((Workflow) enclosingWorkflow).addAction(action);			
			
		} else if (actionType.getNodeName().equals(Resources.PROCESS_ACTION)) {
			logger.info("Action " + name + " is a process action");
			ProcessAction process = new ProcessAction(name, role, enclosingWorkflow, isAutomaticallyInstantiated);
			String actionWorkflowName = actionType.getAttribute(Resources.ACTION_WORKFLOW);
			if (actionWorkflowName == null || actionWorkflowName.isEmpty() || !workflowsNames.contains(actionWorkflowName))
				throw new WorkflowMonitorException("Node " + actionType.getNodeName() + ": " + Resources.ACTION_WORKFLOW + " attribute not found or have invalid value.");
			
			Workflow actionWorkflow = (Workflow) monitor.getWorkflow(actionWorkflowName);
			if (actionWorkflow != null) {
				process.setActionWorkflow(actionWorkflow);
			} else {
				unsolvedActionWorkflows.add(new String[] { enclosingWorkflowName, actionWorkflowName, name });
			}
			
			((Workflow) enclosingWorkflow).addAction(process);
			
		} else {
			throw new WorkflowMonitorException("Node " + actionType.getNodeName() + ": invalid kind of action, expected " + Resources.SIMPLE_ACTION + " or " + Resources.PROCESS_ACTION + ".");
		}
	    
	    logger.info("Action " + name + " correctly parsed");
	}
	
	public void parseActionStatus(Element actionStatusNode, Process process, WorkflowReader workflow) throws WorkflowMonitorException {
		Actor actor = null;
		Calendar terminationTime = null;
		
		if (!actionStatusNode.hasAttributes())
	    	throw new WorkflowMonitorException("Node " + actionStatusNode.getNodeName() + ": no attribute found.");
	    
	    String actionName = actionStatusNode.getAttribute(Resources.NAME);
	    if (actionName == null || actionName.isEmpty())
	    	throw new WorkflowMonitorException("Node " + actionStatusNode.getNodeName() + ": " + Resources.NAME + " attribute not found or have no value.");
	    if (!actionName.matches(Resources.NAME_REGEX))
	    	throw new WorkflowMonitorException("Node " + actionStatusNode.getNodeName() + ": invalid " + Resources.NAME + " format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
	    
	    logger.info("Parsing action status " + actionName + "...");
	    
	    ActionReader action = workflow.getAction(actionName);
	    if (action == null)
	    	throw new WorkflowMonitorException("Node " + actionStatusNode.getNodeName() + ": the corrispondent action in workflow not exists.");
	    
	    String termination = actionStatusNode.getAttribute(Resources.TERMINATION_TIME);
		if (termination != null && !termination.isEmpty()) {
			terminationTime = new GregorianCalendar();
			try {
				terminationTime.setTime(dateFormat.parse(termination));
				terminationTime.setTimeZone(dateFormat.getTimeZone());
			} catch (ParseException e) {
				throw new WorkflowMonitorException("Node " + actionStatusNode.getNodeName() + ": invalid date time format, expected '" + Resources.DATE_FORMAT + "'.");
			}
			if (terminationTime.before(process.getStartTime()))
				throw new WorkflowMonitorException("Node " + actionStatusNode.getNodeName() + ": invalid " + Resources.TERMINATION_TIME + ", it cannot be before the " + Resources.START_TIME + ".");
		}
		
		Node node = actionStatusNode.getFirstChild();
	    boolean found = false;
	    while (node != null) {
	    	if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().equals(Resources.ACTOR)) {
				found = true;
				break;
			}
			node = node.getNextSibling();
		}
	    
	    if (found) {	    	
	    	Element actorNode = (Element) node;
	    	
	    	String name = actorNode.getAttribute(Resources.NAME);
		    if (name == null || name.isEmpty())
		    	throw new WorkflowMonitorException("Node " + actorNode.getNodeName() + ": " + Resources.NAME + " attribute not found or have no value.");
		    if (!name.matches(Resources.ROLE_REGEX))
		    	throw new WorkflowMonitorException("Node " + actorNode.getNodeName() + ": invalid " + Resources.NAME + " format, it must be an alphabetic string.");
		    
		    String role = actorNode.getAttribute(Resources.ROLE);
		    if (role == null || role.isEmpty())
		    	throw new WorkflowMonitorException("Node " + actorNode.getNodeName() + ": " + Resources.ROLE + " attribute not found or have no value.");
		    if (!role.matches(Resources.ROLE_REGEX))
		    	throw new WorkflowMonitorException("Node " + actorNode.getNodeName() + ": invalid " + Resources.ROLE + " format, it must be an alphabetic string.");
		    if (!action.getRole().equals(role))
		    	throw new WorkflowMonitorException("Node " + actorNode.getNodeName() + ": invalid role value, expected " + action.getRole() + ".");
		    
		    actor = new Actor(name, role);
	    }
	    
	    boolean takenInCharge = false, isTerminated = false;
	    
	    String inCharge = actionStatusNode.getAttribute(Resources.TAKEN_IN_CHARGE);
	    if (inCharge != null && !inCharge.isEmpty())
	    	takenInCharge = Boolean.parseBoolean(inCharge);
	    
	    String terminated = actionStatusNode.getAttribute(Resources.IS_TERMINATED); 
	    if (terminated != null && !terminated.isEmpty())
	    	isTerminated = Boolean.parseBoolean(terminated);
	    
	    if (takenInCharge == (actor == null))
	    	throw new WorkflowMonitorException("Node " + actionStatusNode.getNodeName() + ": " + Resources.TAKEN_IN_CHARGE + takenInCharge + " and the " + Resources.ACTOR + actor + " are wrong.");

	    if (isTerminated == (terminationTime == null))
	    	throw new WorkflowMonitorException("Node " + actionStatusNode.getNodeName() + ": " + Resources.IS_TERMINATED + " and the " + Resources.TERMINATION_TIME + " are wrong.");

	    if ((actor == null) && (terminationTime != null))
	    	throw new WorkflowMonitorException("Node " + actionStatusNode.getNodeName() + ": " + Resources.TERMINATION_TIME + " and the " + Resources.ACTOR + " are wrong.");
	    
	    ActionStatus actionStatus = new ActionStatus(actionName, actor, terminationTime);
	    process.addActionStatus(actionStatus);
	    
	    logger.info("Action status " + actionName + " correctly parsed");
	}
	
	/**
	 * Method for saving all workflow names in a {@link HashSet} to let faster checks parsing
	 * the document.
	 * 
	 * @param workflowsList a {@link NodeList} of all the workflows of the document
	 * @throws WorkflowMonitorException
	 */
	private void populateSet(NodeList workflowsList) throws WorkflowMonitorException {
		workflowsNames = new HashSet<String>();
		Element node;
		String name;
		
		logger.info("Populating set of Workflow names...");
		
		int length = workflowsList.getLength();
		if (length > 0) {
			for (int i = 0; i < length; i++) {
				node = (Element) workflowsList.item(i);
				name = node.getAttribute(Resources.NAME);
				if (name == null || name.isEmpty())
					throw new WorkflowMonitorException("Node " + node.getNodeName() + ": " + Resources.NAME + " attribute not found or have no value.");
				if (!name.matches(Resources.NAME_REGEX))
					throw new WorkflowMonitorException("Node " + node.getNodeName() + ": invalid " + Resources.NAME + " format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
				
				if (workflowsNames.contains(name))
					throw new WorkflowMonitorException("Node " + node.getNodeName() + ": found more than one workflow with the same name, name must be unique");
				
				workflowsNames.add(name);
			}
		}
		
		logger.info("population done");
	}
	
	/**
	 * Method for set the correct action workflow reference to each process action.
	 * 
	 * @throws WorkflowMonitorException
	 */
	private void solveActionWorkflows() throws WorkflowMonitorException {
		logger.info("Solving all action Workflow...");
		
		for (String[] unsolved : unsolvedActionWorkflows) {
			String actionWorkflowName = unsolved[ACTION_WORKFLOW_INDEX];
			if (!workflowsNames.contains(actionWorkflowName))
				throw new WorkflowMonitorException("Invalid " + Resources.ACTION_WORKFLOW + " name, it not exist.");
			
			Workflow actionWorkflow 	= (Workflow) monitor.getWorkflow(actionWorkflowName);
			Workflow enclosingWorkflow 	= (Workflow) monitor.getWorkflow(unsolved[ENCLOSING_WORKFLOW_INDEX]);
			ProcessAction processAction = (ProcessAction) enclosingWorkflow.getAction(unsolved[PROCESS_ACTION_INDEX]);
			processAction.setActionWorkflow(actionWorkflow);
		}
		unsolvedActionWorkflows.clear();
		
		logger.info("Action workflow solved");
	}
	
	public static void main(String[] args) throws WorkflowMonitorException {
		XMLWorkflowMonitorParser parser = new XMLWorkflowMonitorParser();
		parser.parse(args[0]);
	}
}
