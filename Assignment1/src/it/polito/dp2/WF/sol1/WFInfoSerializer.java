package it.polito.dp2.WF.sol1;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import it.polito.dp2.WF.ActionReader;
import it.polito.dp2.WF.ActionStatusReader;
import it.polito.dp2.WF.Actor;
import it.polito.dp2.WF.FactoryConfigurationError;
import it.polito.dp2.WF.ProcessActionReader;
import it.polito.dp2.WF.ProcessReader;
import it.polito.dp2.WF.SimpleActionReader;
import it.polito.dp2.WF.WorkflowMonitor;
import it.polito.dp2.WF.WorkflowMonitorException;
import it.polito.dp2.WF.WorkflowMonitorFactory;
import it.polito.dp2.WF.WorkflowReader;


public class WFInfoSerializer {
	
	private static Logger logger = Logger.getLogger(WFInfoSerializer.class.getName());
	
	private WorkflowMonitor monitor;
	private DateFormat dateFormat;
	
	public WFInfoSerializer() throws WorkflowMonitorException {
		Resources.setLogHandler(logger, true);
		try {			
			WorkflowMonitorFactory factory = WorkflowMonitorFactory.newInstance();
			monitor = factory.newWorkflowMonitor();
			dateFormat = new SimpleDateFormat(Resources.DATE_FORMAT);
		} catch (WorkflowMonitorException e) {
			logger.severe(e.getMessage());
			throw e;
		}  catch (Exception e) {
			logger.severe(e.getMessage());
			throw new WorkflowMonitorException(e);
		}
	}
	
	public WFInfoSerializer(WorkflowMonitor monitor) throws WorkflowMonitorException {
		super();
		Resources.setLogHandler(logger, true);
		try {
			this.monitor = monitor;
			dateFormat = new SimpleDateFormat(Resources.DATE_FORMAT);
		}  catch (Exception e) {
			logger.severe(e.getMessage());
			throw new WorkflowMonitorException(e);
		}
	}
	
	public void serialize(String outputFileName) throws ParserConfigurationException, FileNotFoundException, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance ();
		DocumentBuilder builder = factory.newDocumentBuilder ();

		Document document = builder.newDocument ();
		Element rootElement = (Element) document.createElement (Resources.WORKFLOW_MONITOR);
		document.appendChild (rootElement);
		
		workflowsToDOM(document, rootElement);
		processesToDOM(document, rootElement);			
		writeOnFile(outputFileName, document);
	}
	
	private void writeOnFile(String outputFileName, Document document) throws FileNotFoundException, TransformerException {
		logger.info("Writing document on file " + outputFileName + "...");
		
		TransformerFactory xformFactory = TransformerFactory.newInstance ();
		Transformer idTransform = xformFactory.newTransformer ();
		
		Source input  = new DOMSource(document);
		Result output = new StreamResult (new FileOutputStream(outputFileName));
		
		idTransform.setOutputProperty(OutputKeys.INDENT, "yes");
		idTransform.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, Resources.DTD_FILE_NAME);		
		idTransform.transform(input, output);
		
		logger.info("writing completed");
	}
	
	public static void main(String[] args) {
		
		if (args.length != 1) {
			logger.severe("Invalid command line argument (it must be -Doutput=<fileName>.xml)");
			throw new RuntimeException("Invalid command line argument (it must be -Doutput=<fileName>.xml)");
		}
		
		WFInfoSerializer serializer;
		try {
			serializer = new WFInfoSerializer();
			logger.info("Starting serializer...");
			serializer.serialize(args[0]);			
			
		} catch (FactoryConfigurationError e) {
			logger.severe("Could not locate a JAXP factory class");
		} catch (ParserConfigurationException e) {
			logger.severe("Could not locate a JAXP DocumentBuilder class");
		} catch (DOMException e) {
			logger.severe("Error while building the DOM tree");
			logger.severe(e.getMessage());
		} catch (TransformerException e) {
			logger.severe("Unexpected Error during serialization");
			logger.severe(e.getMessage());
		} catch (WorkflowMonitorException e) {
			logger.severe("Unexpected Error during instantiation of data generator");
			logger.severe(e.getMessage());
		} catch (FileNotFoundException e) {
			logger.severe("Unexpected Error during opening the file");
			logger.severe(e.getMessage());
		} finally {			
			logger.info("Serializer terminated.");
		}
	}
	
	private void workflowsToDOM(Document document, Element rootElement) {
		/* Set of the workflows */
		Set<WorkflowReader> workflows = monitor.getWorkflows();
		for (WorkflowReader workflowReader : workflows)
			workflowToDOM(document, workflowReader, rootElement);
	}
	
	private void workflowToDOM(Document document, WorkflowReader workflowReader, Element rootElement) {
		logger.info("Serializing workflow " + workflowReader.getName() + "...");
		
		Element workflow = document.createElement(Resources.WORKFLOW);
		workflow.setAttribute(Resources.NAME, workflowReader.getName());
		
		/* Set of the actions */
		Set<ActionReader> actions = workflowReader.getActions();
		for (ActionReader actionReader : actions)
			actionToDOM(document, actionReader, workflow, rootElement);
		
		logger.info("Workflow " + workflowReader.getName() + " correctly serialized");
		
		rootElement.appendChild(workflow);
	}
	
	private void actionToDOM(Document document, ActionReader actionReader, Element workflow, Element rootElement) {
		StringBuilder stringBuilder = new StringBuilder();
		
		logger.info("Serializing action " + actionReader.getName() + "...");
		
		Element action = document.createElement(Resources.ACTION);
		action.setAttribute(Resources.NAME, actionReader.getName());
		action.setAttribute(Resources.ENCLOSING_WORKFLOW, actionReader.getEnclosingWorkflow().getName());
		action.setAttribute(Resources.ROLE, actionReader.getRole());
		if (actionReader.isAutomaticallyInstantiated())
			action.setAttribute(Resources.IS_AUTOMATICALLY_INSTANTIATED, Boolean.toString(true));
		
		if (actionReader instanceof SimpleActionReader) {
			SimpleActionReader simpleActionReader = (SimpleActionReader) actionReader;
			Element simpleAction = document.createElement(Resources.SIMPLE_ACTION);

			Set<ActionReader> possibleNextActions = simpleActionReader.getPossibleNextActions();
			if (!possibleNextActions.isEmpty()) {
				for (ActionReader nextAction : possibleNextActions)
					stringBuilder.append(nextAction.getName() + " ");
				
				stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(" "));
				simpleAction.setAttribute(Resources.NEXT_ACTIONS, stringBuilder.toString());
				stringBuilder.setLength(0);	// to reuse the same StringBuilder
			}
			action.appendChild(simpleAction);
		}
		if (actionReader instanceof ProcessActionReader) {
			ProcessActionReader processActionReader = (ProcessActionReader) actionReader;
			Element processAction = document.createElement(Resources.PROCESS_ACTION);
			processAction.setAttribute(Resources.ACTION_WORKFLOW, processActionReader.getActionWorkflow().getName());
			action.appendChild(processAction);
		}
		
		logger.info("Action " + actionReader.getName() + " correctly serialized");
		
		workflow.appendChild(action);
	}
	
	private void processesToDOM(Document document, Element rootElement) {
		/* Set of the processes */
		Set<ProcessReader> processes = monitor.getProcesses();
		for (ProcessReader processReader : processes)
			processToDOM(document, processReader, rootElement);
	}
	
	private void processToDOM(Document document, ProcessReader processReader, Element rootElement) {
		logger.info("Serializing process of workflow " + processReader.getWorkflow().getName() + "...");		
		
		Element process = document.createElement(Resources.PROCESS);
		process.setAttribute(Resources.WORKFLOW, processReader.getWorkflow().getName());
		Calendar startTime = processReader.getStartTime();
		dateFormat.setTimeZone(startTime.getTimeZone());
		process.setAttribute(Resources.START_TIME, dateFormat.format(startTime.getTime()));
		
		/* Set of the actions status */
		List<ActionStatusReader> actionsStatus = processReader.getStatus();
		for (ActionStatusReader actionStatusReader : actionsStatus)
			actionStatusToDOM(document, actionStatusReader, process, rootElement);		
		
		logger.info("Process of workflow " + processReader.getWorkflow().getName() + " started at " + dateFormat.format(startTime.getTime()) + " correctly serialized");
		
		rootElement.appendChild(process);
	}
	
	private void actionStatusToDOM(Document document, ActionStatusReader actionStatusReader, Element process, Element rootElement) {
		logger.info("Serializing action status " + actionStatusReader.getActionName() + "...");
		
		Element actionStatus = document.createElement(Resources.ACTION_STATUS);
		actionStatus.setAttribute(Resources.NAME, actionStatusReader.getActionName());
		if (actionStatusReader.isTakenInCharge())
			actionStatus.setAttribute(Resources.TAKEN_IN_CHARGE, Boolean.toString(true));
		if (actionStatusReader.isTerminated())
			actionStatus.setAttribute(Resources.IS_TERMINATED, Boolean.toString(true));
		Calendar terminationTime = actionStatusReader.getTerminationTime();
		if (terminationTime != null) {
			dateFormat.setTimeZone(terminationTime.getTimeZone());
			actionStatus.setAttribute(Resources.TERMINATION_TIME, dateFormat.format(terminationTime.getTime()));						
		}
		
		Actor processActor = actionStatusReader.getActor();
		if (processActor != null) {
			Element actor = document.createElement(Resources.ACTOR);
			actor.setAttribute(Resources.NAME, processActor.getName());
			actor.setAttribute(Resources.ROLE, processActor.getRole());
			actionStatus.appendChild(actor);						
		}
		
		logger.info("Process of action status " + actionStatusReader.getActionName() + " correctly serialized");
		
		process.appendChild(actionStatus);
	}
}
