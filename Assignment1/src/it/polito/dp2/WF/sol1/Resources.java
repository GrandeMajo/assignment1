package it.polito.dp2.WF.sol1;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class Resources {
	public static final int MONDAY 		= 0;
	public static final int TUESDAY		= 1;
	public static final int WEDNESDAY 	= 2;
	public static final int THURSDAY 	= 3;
	public static final int FRIDAY 		= 4;
	public static final int SATURDAY 	= 5;
	public static final int SUNDAY 		= 6;

	public static final String WORKFLOW_MONITOR					= "workflowMonitor";
	public static final String WORKFLOW							= "workflow";
	public static final String PROCESS							= "process";
	public static final String PROCESSES						= "processes";
	public static final String ACTION							= "action";
	public static final String ACTION_STATUS					= "actionStatus";
	public static final String SIMPLE_ACTION					= "simpleAction";
	public static final String PROCESS_ACTION					= "processAction";
	public static final String NAME								= "name";
	public static final String ROLE								= "role";
	public static final String ENCLOSING_WORKFLOW				= "enclosingWorkflow";
	public static final String ACTION_WORKFLOW					= "actionWorkflow";
	public static final String IS_AUTOMATICALLY_INSTANTIATED	= "isAutomaticallyInstantiated";
	public static final String NEXT_ACTIONS						= "nextActions";
	public static final String START_TIME						= "startTime";
	public static final String STATUS							= "status";
	public static final String ACTOR							= "actor";
	public static final String TERMINATION_TIME					= "terminationTime";
	public static final String TAKEN_IN_CHARGE					= "isTakenInCharge";
	public static final String IS_TERMINATED					= "isTerminated";
	
	public static final String XML_FILE_NAME					= "file.xml";
	public static final String DTD_FILE_NAME					= "wfInfo.dtd";
	public static final String LOG_FILE_NAME					= "assignment1.log";

	public static final String DATE_FORMAT	= "dd/MM/yyyy HH:mm:ss Z";
	public static final String NAME_REGEX	= "[a-zA-Z][a-zA-Z0-9]*";
	public static final String ROLE_REGEX	= "[a-zA-Z ]+";

	public static void setLogHandler(Logger logger, boolean append) {
		try {
			logger.setLevel(Level.ALL);
			FileHandler handler = new FileHandler("./" + LOG_FILE_NAME, append);
			logger.addHandler(handler);
			handler.setFormatter(new SimpleFormatter());			
		} catch (Exception e) {
			logger.warning("Error setting log handler file: " + e.getMessage());
		}
	}
}
